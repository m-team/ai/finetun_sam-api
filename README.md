# finetun_sam-api

[![Build Status](https://jenkins.indigo-datacloud.eu/buildStatus/icon?job=Pipeline-as-code/DEEP-OC-org/finetun_sam-api/master)](https://jenkins.indigo-datacloud.eu/job/Pipeline-as-code/job/DEEP-OC-org/job/finetun_sam-api/job/master)

Integrate an API that allows fine-tuning of the 'segment anything' model on your custom dataset using bounding boxes, with a focus on enhancement and promotion.

The SAM model comprises an image encoder, prompt encoder, and mask decoder. In this application, we fix the weights of the image encoder and prompt encoder while training the mask decoder within the SAM model.
## Install the API and the external submodule requirement

To launch the API, first, install the package, and then run [DeepaaS](https://github.com/indigo-dc/DEEPaaS):

> ![warning](https://img.shields.io/badge/Warning-red.svg) **Warning**: If you are using a virtual environment, make sure you are working with the last version of pip before installing the package. Use `pip install --upgrade pip` to upgrade pip.

```bash
git clone https://github.com/falibabaei/finetun_sam-api
cd finetun_sam-api
pip install -e .
deepaas-run --listen-ip 0.0.0.0
```
><span style="color:Blue">**Note:**</span> Before installing the API and submodule requirements, please make sure to install the following system packages: `gcc`, `unzip`, and `libgl1` as well. These packages are essential for a smooth installation process and proper functioning of the framework.

```bash
apt update
apt install -y unzip
apt install -y gcc
apt install -y libgl1
apt install -y libglib2.0-0
```

## Project structure

```bash
├── Jenkinsfile             <- Describes basic Jenkins CI/CD pipeline
├── Dockerfile              <- Steps to build a DEEPaaS API Docker image
├── LICENSE                 <- License file
├── README.md               <- The top-level README for developers using this project.
├── VERSION                 <- Version file indicating the version of the model
│
├── finetun_sam
│   ├── README.md           <- Instructions on how to integrate your model with DEEPaaS.
│   ├── __init__.py         <- Makes <your-model-source> a Python module
│   ├── ...                 <- Other source code files
│   └── config.py           <- Module to define CONSTANTS used across the AI-model python package
│
├── api                     <- API subpackage for the integration with DEEP API
│   ├── __init__.py         <- Makes api a Python module, includes API interface methods
│   ├── config.py           <- API module for loading configuration from environment
│   ├── responses.py        <- API module with parsers for method responses
│   ├── schemas.py          <- API module with definition of method arguments
│   └── utils.py            <- API module with utility functions
│
├── data                    <- Data subpackage for the integration with DEEP API
│
├── docs                    <- A default Sphinx project; see sphinx-doc.org for details
│
├── models                  <- Folder to store your models
│
├── notebooks               <- Jupyter notebooks. Naming convention is a number (for ordering),
│                              the creator's initials (if many user development),
│                              and a short `_` delimited description, e.g.
│                              `1.0-jqp-initial_data_exploration.ipynb`.
│
├── references              <- Data dictionaries, manuals, and all other explanatory materials.
│
├── reports                 <- Generated analysis as HTML, PDF, LaTeX, etc.
│   └── figures             <- Generated graphics and figures to be used in reporting
│
├── requirements-dev.txt    <- Requirements file to install development tools
├── requirements-test.txt   <- Requirements file to install testing tools
├── requirements.txt        <- Requirements file to run the API and models
│
├── pyproject.toml          <- Makes project pip installable (pip install -e .)
│
├── tests                   <- Scripts to perform code testing
│   ├── configurations      <- Folder to store the configuration files for DEEPaaS server
│   ├── conftest.py         <- Pytest configuration file (Not to be modified in principle)
│   ├── data                <- Folder to store the data for testing
│   ├── models              <- Folder to store the models for testing
│   ├── test_deepaas.py     <- Test file for DEEPaaS API server requirements (Start, etc.)
│   ├── test_metadata       <- Tests folder for model metadata requirements
│   ├── test_predictions    <- Tests folder for model predictions requirements
│   └── test_training       <- Tests folder for model training requirements
│
└── tox.ini                 <- tox file with settings for running tox; see tox.testrun.org
```
## Dataset Structure

For consistency and clarity, adhere to the following structure for your dataset:

### Folder Organization:
- The dataset root directory should consist of two primary folders:
  - `train`: This directory will store the train dataset.
    - `images`: This directory will store the images related to the dataset.
    - `masks`: Here, you'll find the corresponding masks for objects in the images.
    -  `prompt`(optional): Contains a `prompt.json` file, which is a dictionary where the keys are the names of image files and the values are lists of bounding boxes with `[x_min, y_min, x_max, y_max]`.
### Mask Representation:
- Each mask should be represented as an image.
- Pixels within the mask image that belong to an object should be labeled with an integer value. 
### Bonding box as prompt

If `prompt.json` does not exist, the bounding box will be automatically generated using `torchvision`.

> **Note:**  You can use the `coco_to_masks` function within `finetun_sam-api/finetun_sam/utils.py` to convert a COCO segmentation JSON file into masks and generate bounding boxes from it.


# Resources used to create this API:

- [MedSAM repository](https://github.com/bowang-lab/MedSAM).
- [Fine_tune_SAM_(segment_anything)_on_a_custom_dataset](https://colab.research.google.com/github/NielsRogge/Transformers-Tutorials/blob/master/SAM/Fine_tune_SAM_(segment_anything)_on_a_custom_dataset.ipynb#scrollTo=nAZ7M__ncb8Y)