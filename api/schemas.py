"""Module for defining custom web fields to use on the API interface.
This module is used by the API server to generate the input form for the
prediction and training methods. You can use any of the defined schemas
to add new inputs to your API.

The module shows simple but efficient example schemas. However, you may
need to modify them for your needs.
"""

import marshmallow
from webargs import ValidationError, fields, validate
import json
from finetun_sam.api import config, responses, utils

from marshmallow import Schema, fields, validate


class PromptField(fields.Field):
    def __init__(self, *args, **kwargs):

        self.metadata = kwargs.get("metadata", {})
        super().__init__(*args, **kwargs)

    def _deserialize(self, value, attr, data, **kwargs):
        try:
            return json.loads(value)
        except json.JSONDecodeError as err:
            raise ValidationError(f"Invalid JSON: `{err}`")

    def _validate(self, value):
        if not isinstance(value, list):
            raise ValidationError(
                "`prompt` must be a list of dictionaries."
            )
        for item in value:
            if not isinstance(item, list):
                raise ValidationError(
                    "Each item in the list must be a list."
                )
            if len(item) != 4:
                raise ValidationError(
                    "Each item in the list should be a list of"
                    " coordinate of the bounding box with [x_min, y_min, x_max, y_max]."
                )
            for val in item:
                if not isinstance(val, int):
                    raise ValidationError(
                        f"Value of bounding box coordinate must be an integer."
                    )


def validate_prompt(data):
    # Custom validation logic here if needed
    pass

class PromptField(fields.Field):
    def __init__(self, *args, **kwargs):
        metadata_msg = {
            'Bounding box prompt. Should be a list. Each item in the list',
            ' should be a list of coordinatesof the bounding box with [x_min, y_min, x_max, y_max]. ',
            'Example:[[0, 0, 100, 100], [255, 350, 400, 550]]'
        }
        self.metadata = kwargs.get("metadata",metadata_msg  )
        super().__init__(*args, **kwargs)

    def _deserialize(self, value, attr, data, **kwargs):
        try:
            return json.loads(value)
        except json.JSONDecodeError as err:
            raise ValidationError(f"Invalid JSON: `{err}`")

    def _validate(self, value):
        if not isinstance(value, list):
            raise ValidationError("`prompt` must be a list of dictionaries.")
        for item in value:
            if not isinstance(item, list):
                raise ValidationError("Each item in the list must be a list.")
            if len(item)!= 4:
                raise ValidationError("Each item in the list should be a list of"
                " coordinate of the bounding box with [x_min, y_min, x_max, y_max].") 
            for   val in item:
                if not isinstance(val, int):
                    raise ValidationError(f"Value of bounding box coordinate must be an integer.")
        
     
def validate_prompt(data):
    # Custom validation logic here if needed
    pass        
class PredArgsSchema(marshmallow.Schema):
    """Prediction arguments schema for api.predict function."""

    class Meta:
        ordered = True
    
 

    input_file = fields.Field(
        metadata={
            "description": "File with np.arrays for predictions.",
            "type": "file",
            "location": "form",
        },
        required=True,
    )

    timestamp = fields.Str(
        metadata={
            "description": "String or path identifying"
            "the model directory. If none is provided, "
            "the 'facebook/sam-vit-base' model will be loaded.",
        },
        required=False,
        load_default=None,
    )
    prompt = PromptField(
        required=False,
        metadata={
            "description": "Bounding box prompt. Should be a list. Each item in the list"
            " should be a list of coordinates of the bounding box with \n [x_min, y_min, x_max, y_max]. \n"
            "Example:[[0, 0, 100, 100], [255, 350, 400, 550]]\n"
            "if not provided, the pretrain yolov8 will be loaded to generate bbox."
        },
        load_default=None,
    )

    yolo_model = fields.Str(
        metadata={
            "description": "Path to a custom YOLOv8 pretrained model (.pth)."
            " If 'prompt' is not provided, you can load your "
            "custom YOLOv8 pretrained model to find the bounding boxes for "
            "you. If not provided, the pretrained yolov8n will be loaded."
        },
        required=False,
        load_default=None,
    )
    device = fields.Str(
        metadata={
            "description": 'Device to run on, e.g., "cuda:0" or "cpu"',
        },
        load_default="cuda:0",
    )
    show_boxes = fields.Bool(
        metadata={
            "description": "shows your bbox on your image.",
        },
        load_default=False,
    )
    multimask_output = fields.Bool(
        metadata={
            "description": "If the output is multimask.",
        },
        load_default=False,
    )
    accept = fields.String(
        metadata={
            "description": "Return format for method response.",
            "location": "headers",
        },
        required=True,
        validate=validate.OneOf(list(responses.content_types)),
    )


class TrainArgsSchema(marshmallow.Schema):
    """Training arguments schema for api.train function."""

    class Meta:
        ordered = True

    checkpoint = fields.Str(
        metadata={
            "description": "String/Path identification for models.",
        },
        validate=validate.OneOf(
            [
                "facebook/sam-vit-base",
                "facebook/sam-vit-huge",
                "facebook/sam-vit-large",
            ]
        ),
        required=True,
    )

    data_path = fields.Str(
        metadata={
            "description": "Path to the training dataset.",
        },
        required=True,
    )
    # prompt_path = fields.Str(
    #      metadata={
    #         "description": "Path to the bboxs. Should be a json file "
    #          "containing a list of lists of coordinates of bboxes.\n"
    #          " Example: [x_min, y_min, x_max, y_max]"
    #          "If not specified, the bbox is generated automatically."
    #      },
    #      required=False,
    #      load_default= None
    # )

    epochs = fields.Integer(
        metadata={
            "description": "Number of epochs to train the model.",
        },
        required=False,
        load_default=1,
        validate=validate.Range(min=1),
    )

    batch_size = fields.Integer(
        metadata={
            "description": "Number of batch size.",
        },
        required=False,
        load_default=1,
        validate=validate.Range(min=1),
    )
    lr = fields.Float(
        metadata={
            "description": "learning rate.",
        },
        required=False,
        load_default=0.01,
    )
    weight_decay = fields.Float(
        metadata={
            "description": "weight decay.",
        },
        required=False,
        load_default=0.01,
    )
    resume = fields.Str(
        metadata={
            "description": "Path to a ckpt that you want to resume training from it.",
        },
        required=False,
        load_default=None,
    )
    imgsz = fields.List(
        fields.Int(),
        validate=validate.Length(max=2),
        load_default=[256],
        metadata={
            "description": "image size as scalar or (h, w) list,"
            " i.e. (640, 480). The load deafult is 256.",
        },
    )
    do_convert_rgb = fields.Bool(
        metadata={
            "description": "Convert the input image to RGB.",
        },
        load_default=True,
    )

    longest_edge = fields.Integer(
        metadata={
            "description": "length of longest edge of the input image.",
        },
        required=False,
        load_default=540,
        validate=validate.Range(min=1),
    )

    use_amp = fields.Bool(
        metadata={
            "description": "Use AMP.",
        },
        load_default=False,
    )
    device = fields.Str(
        metadata={
            "description": 'Device to run on, e.g., "cuda:0" or "cpu"',
        },
        load_default="cuda:0",
    )
    #  trainable_components = fields.Str(
    #    metadata={
    #        "description": "Specifies the components of the SAM model to be trained. "
    #         "The SAM model comprises an image encoder, prompt encoder, "
    #        "and mask decoder. By default, the mask decoder is trained. "
    #        "You can specify to train one or more of the encoders.",
    #    },
    #   required=False,
    #     load_default="['vision_encoder', 'prompt_encoder']",
    #   )

    num_workers = fields.Integer(
        metadata={
            "description": "Number of batch size.",
        },
        required=False,
        load_default=4,
        validate=validate.Range(min=1),
    )

 
