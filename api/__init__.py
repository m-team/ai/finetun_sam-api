"""Package to create dataset, build training and prediction pipelines.

This file should define or import all the functions needed to operate the
methods defined at finetun_sam/api.py. Complete the TODOs
with your own code or replace them importing your own functions.
For example:
```py
from your_module import your_function as predict
from your_module import your_function as training
https://github.com/NielsRogge/Transformers-Tutorials/blob/master/SAM/Fine_tune_SAM_(segment_anything)_on_a_custom_dataset.ipynb
```
"""

# TODO: add your imports here
import logging
import os
from aiohttp.web import HTTPException
from datetime import datetime
from tqdm import tqdm
from statistics import mean
from PIL import Image
import json
import argparse
import tempfile
import shutil
import ast
import monai
import numpy as np
from transformers import SamProcessor, SamImageProcessor
from transformers import SamModel
from torch.utils.data import DataLoader
import torch
from torch import nn
from ultralytics import YOLO
from deepaas.model.v2.wrapper import UploadedFile
from torch.nn.functional import threshold, normalize
from torch.nn.utils.rnn import pad_sequence


from finetun_sam.api.utils import (
    LOADDataset,
    DiceLoss,
    validate_and_modify_path,
    add_arguments_from_schema,
    CustomDataLoader,
    collate_fn,
)
from finetun_sam.api import config, schemas, utils, responses


logger = logging.getLogger(__name__)
logger.setLevel(config.LOG_LEVEL)


def get_metadata():
    """Returns a dictionary containing metadata information about the module.

    Raises:
        HTTPException: Unexpected errors aim to return 50X

    Returns:
        A dictionary containing metadata information required by DEEPaaS.
    """
    try:  # Call your AI model metadata() method
        logger.info("Collecting metadata from: %s", config.MODEL_NAME)
        metadata = {
            "author": config.API_METADATA.get("authors"),
            "author-email": config.API_METADATA.get("author-emails"),
            "description": config.API_METADATA.get("summary"),
            "license": config.API_METADATA.get("license"),
            "version": config.API_METADATA.get("version"),
        }
        logger.debug("Package model metadata: %s", metadata)
        return metadata
    except Exception as err:
        raise HTTPException(reason=err) from err


def warm(**kwargs):
    """Main/public method to start up the model"""
    # if necessary, start the model
    pass


@utils.predict_arguments(schema=schemas.PredArgsSchema)
def predict(**options):
    """Main/public method to perform prediction"""
    try:
        logger.debug("Predict with args: %s", options)
        print("options: ", options)
        timestamp = options["timestamp"]

        if timestamp is not None:
            model_path = os.path.join(config.MODELS_PATH, timestamp)
            if not os.path.exists(model_path):
                raise FileNotFoundError(
                    f"Model path '{model_path}' does not exist."
                )
            model = SamModel.from_pretrained(model_path)
            processor = SamImageProcessor.from_pretrained(model_path)
        else:

            print("download model from pretrained model")
            model = SamModel.from_pretrained("facebook/sam-vit-base")
            processor = SamProcessor.from_pretrained(
                "facebook/sam-vit-base"
            )
        device = (
            "cuda"
            if options["device"] == "cuda"
            and torch.cuda.is_available()
            else "cpu"
        )
        model.eval()

        predictions = []
        with tempfile.TemporaryDirectory() as tmpdir:
            for f in [options["input_file"]]:
                shutil.copy(
                    f.filename, tmpdir + "/" + f.original_filename
                )

            options["input_file"] = [
                os.path.join(tmpdir, t) for t in os.listdir(tmpdir)
            ]

            for file_path in tqdm(options["input_file"]):
                with torch.no_grad():

                    # Read and preprocess the input image
                    image = Image.open(file_path).convert("RGB")
                    width, height = image.size
                    if options["prompt"] is None:
                        if options["yolo_model"] is not None:
                            yolo = YOLO(options["yolo_model"])
                        else:
                            yolo = YOLO("yolov8n.pt")
                        results = yolo.predict(
                            source=file_path, conf=0.25
                        )

                        options["prompt"] = results[
                            0
                        ].boxes.xyxy.tolist()
                        if options["prompt"] is None:
                            options["prompt"] = [
                                [0, 0, width, height]
                            ]
                    print("prompt shape is: ", options["prompt"])
                    inputs = processor(
                        images=image,
                        return_tensors="pt",
                        input_boxes=[[options["prompt"]]],
                        padding=True,
                    )
                    inputs = {
                        key: value.to(device)
                        for key, value in inputs.items()
                    }

                    image_embeddings = model.get_image_embeddings(
                        inputs["pixel_values"]
                    )
                    inputs.pop("pixel_values", None)
                    inputs.update(
                        {"image_embeddings": image_embeddings}
                    )
                    outputs = model(
                        **inputs,
                        multimask_output=options["multimask_output"],
                    )

                    masks = (
                        processor.image_processor.post_process_masks(
                            outputs.pred_masks.cpu(),
                            inputs["original_sizes"].cpu(),
                            inputs["reshaped_input_sizes"].cpu(),
                        )
                    )

                    scores = outputs.iou_scores
                    print("scores shape is: ", scores.shape)
                    predictions.append(
                        {
                            "masks": masks,
                            "scores": scores,
                            "file_path": file_path,
                        }
                    )

        logger.debug(f"[predict()]: {predictions}")

        return responses.content_types[options["accept"]](
            image, masks, scores, **options
        )

    except Exception as err:
        logger.critical(err, exc_info=True)
        raise HTTPException(reason=err) from err


@utils.train_arguments(schema=schemas.TrainArgsSchema)
def train(**options):
    """Main/public method to perform training"""
    try:
        logger.info("Training model...")
        logger.debug("Train with args: %s", options)
        timestamp = datetime.now().strftime("%Y-%m-%d_%H%M%S")
        model_path = os.path.join(config.MODELS_PATH, timestamp)
        if not os.path.exists(model_path):
            os.makedirs(model_path)
        processor = SamProcessor.from_pretrained(
            options["checkpoint"]
        )
        processor.size = {"longest_edge": options["longest_edge"]}
        processor.do_convert_rgb = options["do_convert_rgb"]

        imgsz = (
            [options["imgsz"], options["imgsz"]]
            if len(options["imgsz"]) == 1
            else options["imgsz"]
        )
        processor.pad_size = {"height": imgsz[0], "width": imgsz[1]}
        options["data_path"] = validate_and_modify_path(
            options["data_path"], config.DATA_PATH
        )

        processor.save_pretrained(model_path)
       # dataset = load_dataset("nielsr/breast-cancer", split="train")

        train_data = LOADDataset(
            data_root=os.path.join(options["data_path"], "train"),
            processors=processor,
        )
        train_dataloader = CustomDataLoader(
            train_data,
            batch_size=options["batch_size"],
            num_workers=options["num_workers"],
            shuffle=True,
            collate_fn=collate_fn,
        )
        #

        print(
            "Number of training samples: ",
            len(train_data),
            #  "\nNumber of validation samples: ",
            #  len(valid_data),
        )

        image_dataloaders = {
            "train": train_dataloader,
            #  "test": valid_dataloader,
        }
        model = SamModel.from_pretrained(options["checkpoint"])

        optimizer = torch.optim.Adam(
            model.mask_decoder.parameters(),
            lr=options["lr"],
            weight_decay=options["weight_decay"],
        )

        device = (
            torch.device(f"cuda:{0}")
            if torch.cuda.is_available()
            else "cpu"
        )
        print("The device is", device)
        model.to(device)

        start_epoch = 0

        dice_loss = monai.losses.DiceCELoss(
            sigmoid=True, squared_pred=True, reduction="mean"
        )
        if options["resume"] is not None:
            validate_and_modify_path(
                config.MODELS_PATH, options["resume"]
            )
            if os.path.isfile(options["resume"]):
                checkpoint = torch.load(
                    options["resume"], map_location=device
                )
                start_epoch = checkpoint["epoch"] + 1
                model.load_state_dict(checkpoint["model"])
                optimizer.load_state_dict(checkpoint["optimizer"])
        if options["use_amp"]:
            scaler = torch.cuda.amp.GradScaler()

        best_loss = 1e5
        for epoch in range(
            start_epoch, options["epochs"] + start_epoch
        ):
            print(
                "Epoch {}/{}".format(
                    epoch, options["epochs"] + start_epoch
                )
            )
            for phase in ["train"]:  # "test"]:
                epoch_losses = []
                if phase == "train":
                    model.train()
                else:
                    model.eval()

                for batch in tqdm(image_dataloaders[phase]):
                    # forward pass

                    pixel_values = batch["pixel_values"].to(device)
                    # is used when there are different numbers of bbox for each images
                    batch["input_boxes"] = pad_sequence(
                        batch["input_boxes"],
                        batch_first=True,
                        padding_value=0,
                    )
                    outputs = model(
                        pixel_values=pixel_values,
                        input_boxes=batch["input_boxes"].to(device),
                        multimask_output=False,
                    )

                    ground_truth_masks = (
                        batch["ground_truth_mask"].float().to(device)
                    )

                    ## Sometimes sam output more than one mask for one image. we get the best iou
                    predicted_masks = outputs.pred_masks.squeeze(1)

                    best_index = torch.argmax(
                        outputs.iou_scores, dim=1
                    ).squeeze(1)
                    batch_indices = torch.arange(
                        predicted_masks.shape[0]
                    )
                    predicted_masks = predicted_masks[
                        batch_indices, best_index
                    ].squeeze(1)

                    # now upsample the prediction mask to the original size
                    upsampled_pred = nn.functional.interpolate(
                        predicted_masks.unsqueeze(1),
                        ground_truth_masks.shape[1:],
                        mode="nearest",
                    ).squeeze(1)

                    loss = dice_loss(
                        upsampled_pred,
                        ground_truth_masks,
                    )
                    if phase == "train":
                        if options["use_amp"]:
                            scaler.scale(loss).backward()
                            scaler.step(optimizer)
                            scaler.update()
                        else:
                            optimizer.zero_grad()
                            loss.backward()
                            optimizer.step()

                    epoch_losses.append(loss.item())

                print(
                    "{} Mean loss: {:.4f}  ".format(
                        phase, mean(epoch_losses)
                    )
                )
                if phase == "train":
                    checkpoint = {
                        "model": model.state_dict(),
                        "optimizer": optimizer.state_dict(),
                        "epoch": epoch,
                    }

                    torch.save(
                        checkpoint,
                        os.path.join(model_path, "model_last.pth"),
                    )
                    model.save_pretrained(model_path)
                    if loss < best_loss:
                        best_loss = loss
                        checkpoint = {
                            "model": model.state_dict(),
                            "optimizer": optimizer.state_dict(),
                            "epoch": epoch,
                        }

                        torch.save(
                            model.state_dict(),
                            os.path.join(
                                model_path, "model_best.pth"
                            ),
                        )
        return {
            f"The model was trained successfully and was saved to: \
                { model_path }"
        }

    except Exception as err:
        logger.critical(err, exc_info=True)
        raise HTTPException(reason=err) from err


def main():
    """
    Runs above-described methods from CLI
    uses: python3 path/to/api.py method --arg1 ARG1_VALUE
    --arg2 ARG2_VALUE
    """

    method_dispatch = {
        "get_metadata": get_metadata,
        "predict": predict,
        "train": train,
    }

    chosen_method = args.method
    logger.debug("Calling method: %s", chosen_method)
    if chosen_method in method_dispatch:
        method_function = method_dispatch[chosen_method]

        if chosen_method == "get_metadata":
            results = method_function()
        else:
            logger.debug("Calling method with args: %s", args)
            del vars(args)["method"]
            if hasattr(args, "input"):
                file_extension = os.path.splitext(args.input)[1]
                args.input = UploadedFile(
                    "input",
                    args.input,
                    "application/octet-stream",
                    f"input{file_extension}",
                )
            results = method_function(**vars(args))

        print(json.dumps(results))
        logger.debug("Results: %s", results)
        return results
    else:
        print("Invalid method specified.")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Model parameters", add_help=False
    )
    cmd_parser = argparse.ArgumentParser()
    subparsers = cmd_parser.add_subparsers(
        help='methods. Use "api.py method --help" to get more info',
        dest="method",
    )
    get_metadata_parser = subparsers.add_parser(
        "get_metadata", help="get_metadata method", parents=[parser]
    )

    predict_parser = subparsers.add_parser(
        "predict", help="commands for prediction", parents=[parser]
    )

    add_arguments_from_schema(
        schemas.PredArgsSchema(), predict_parser
    )

    train_parser = subparsers.add_parser(
        "train", help="commands for training", parents=[parser]
    )

    add_arguments_from_schema(schemas.TrainArgsSchema(), train_parser)

    args = cmd_parser.parse_args()

    main()
'''
    args = {
        "checkpoint": "facebook/sam-vit-base",
        "data_path": "/hkfs/home/haicore/scc/se1131/Fish_Dataset_Instance_segmentation-1",
        "epochs": 50,
        "batch_size": 2,
        "lr": 1e-5,
        "weight_decay": 0,
        "resume": None,
        "use_amp": False,
        "num_workers": 8,
        "device": "cuda:0",
        "imgsz": [640],
        "longest_edge": 640,
        "do_convert_rgb": True,
        "trainable_components": "['vision_encoder', 'prompt_encoder']",
    }
    train(**args)
'''