"""Package to create datasets, pipelines and other utilities.

You can use this module for example, to write all the functions needed to
operate the methods defined at `__init__.py` or in your scripts.

All functions here are optional and you can add or remove them as you need.
"""

import glob
import io
import logging


# from finetun_sam import config
from torch.utils.data import Dataset

import io


import os
import sys
import numpy as np
import torch
import matplotlib.pyplot as plt
import torchvision.transforms.functional as F
from torchvision.ops import masks_to_boxes
from torchvision.io import read_image, ImageReadMode
import torchvision
from marshmallow import fields
import torchvision.transforms.functional as TF
import json
import random
import torch

logger = logging.getLogger(__name__)

def generate_arguments(schema):
    """Function to generate arguments for DEEPaaS using schemas."""
    def arguments_function():  # fmt: skip
        logger.debug("Web args schema: %s", schema)
        return schema().fields
    return arguments_function


def predict_arguments(schema):
    """Decorator to inject schema as arguments to call predictions."""

    def inject_function_schema(func):
        get_args = generate_arguments(schema)
        sys.modules[func.__module__].get_predict_args = get_args
        return func  # Decorator that returns same function

    return inject_function_schema


def train_arguments(schema):
    """Decorator to inject schema as arguments to perform training."""
    def inject_function_schema(func):  # fmt: skip
        get_args = generate_arguments(schema)
        sys.modules[func.__module__].get_train_args = get_args
        return func  # Decorator that returns same function
    return inject_function_schema

def generate_arguments(schema):
    """Function to generate arguments for DEEPaaS using schemas."""
    def arguments_function():  # fmt: skip
        logger.debug("Web args schema: %s", schema)
        return schema().fields
    return arguments_function


def predict_arguments(schema):
    """Decorator to inject schema as arguments to call predictions."""

    def inject_function_schema(func):
        get_args = generate_arguments(schema)
        sys.modules[func.__module__].get_predict_args = get_args
        return func  # Decorator that returns same function

    return inject_function_schema


def train_arguments(schema):
    """Decorator to inject schema as arguments to perform training."""
    def inject_function_schema(func):  # fmt: skip
        get_args = generate_arguments(schema)
        sys.modules[func.__module__].get_train_args = get_args
        return func  # Decorator that returns same function
    return inject_function_schema


def show(imgs):
    if not isinstance(imgs, list):
        imgs = [imgs]
    fix, axs = plt.subplots(ncols=len(imgs), squeeze=False)
    for i, img in enumerate(imgs):
        img = img.detach()
        img = F.to_pil_image(img)
        axs[0, i].imshow(np.asarray(img))
        axs[0, i].set(
            xticklabels=[], yticklabels=[], xticks=[], yticks=[]
        )


def get_bboxes(ground_truth_mask):
    obj_ids = torch.unique(ground_truth_mask)
    # first id is the background, so remove it.
    obj_ids = obj_ids[1:]
    # split the color-encoded mask into a set of boolean masks.
    # Note that this snippet would work as well if the masks were float values instead of ints.
    masks = ground_truth_mask == obj_ids[:, None, None]
    boxes = masks_to_boxes(masks)
    return boxes.tolist()


class LOADDataset1(Dataset):
    def __init__(
        self,
        dataset,
        processors=None,
        img_size=640,
    ):

        self.processors = processors

        self.dataset = dataset

        # prompt_path = os.path.join(data_root, "prompt", "prompt.json")
        # if os.path.exists(prompt_path):
        # with open(prompt_path, "r") as file:

        # self.prompt = json.load(file)

    def __len__(self):
        return len(self.dataset)

    def __getitem__(self, index):
        item = self.dataset[index]

        # print("********", img_filename)
        # image = Image.open(img_filename).convert("RGB")
        image = item["image"]
        # image = read_image(image)
        # image_shape = image.shape

        # Extract width and height
        # width = image_shape[1]
        # height = image_shape[2]
        # image = torch.from_numpy(image)
        # image = F.to_tensor(image)

        # mask_filename = self.mask_list[index]

        ground_truth_mask = np.array(item["label"])

        # ground_truth_mask = (
        #    ground_truth_mask.clone().detach().squeeze(0)
        # )

        # ground_truth_mask = F.convert_image_dtype(
        #     ground_truth_mask, dtype=torch.float
        # )
        # We get the unique colors, as these would be the object ids.
        #  if self.aug_option:
        #    image, ground_truth_mask = self.transform(
        #      image, ground_truth_mask
        # )

        # if self.prompt is not None:
        #    file_name= os.path.basename(img_filename)
        #  prompt = next((d[file_name] for d in self.prompt if file_name in d), None)

        # else:
        ground_truth_mask = torch.from_numpy(ground_truth_mask)
        prompt = get_bboxes(ground_truth_mask)

        inputs = self.processors(
            image,
            input_boxes=[prompt],
            return_tensors="pt",
            do_rescale=False,
        )

        inputs = {k: v.squeeze(0) for k, v in inputs.items()}
        # print("Image size:", image.shape)
        # print("Ground truth mask size:", ground_truth_mask.shape)
        # for k, v in inputs.items():
        #  print(f"{k} size:", v.shape)
        # add ground truth segmentation

        inputs["ground_truth_mask"] = (ground_truth_mask > 0).float()

        return inputs


class LOADDataset(Dataset):
    def __init__(
        self,
        data_root,
        processors=None,
        img_size=640,
    ):
        self.data_root = data_root
        self.img_path = os.path.join(data_root, "images")
        self.mask_path = os.path.join(data_root, "masks")
        self.processors = processors
        self.file_list = sorted(
            glob.glob(self.img_path + "/*.png")
            + glob.glob(self.img_path + "/*.jpg")
        )
        self.mask_list = sorted(
            glob.glob(self.mask_path + "/*.png")
            + glob.glob(self.mask_path + "/*.jpg")
        )

        self.img_size = img_size

        prompt_path = os.path.join(data_root, "prompt", "prompt.json")
        if os.path.exists(prompt_path):

            with open(prompt_path, "r") as file:

                self.prompt = json.load(file)
        else:
            self.prompt = None

    def __len__(self):
        return len(self.file_list)

    def __getitem__(self, index):
        img_filename = self.file_list[index]

        image = read_image(img_filename)
        image_shape = image.shape

        # Extract width and height
        width = image_shape[1]
        height = image_shape[2]

        image = F.convert_image_dtype(image, dtype=torch.float)

        mask_filename = self.mask_list[index]

        ground_truth_mask = read_image(
            mask_filename, mode=ImageReadMode.GRAY
        )

        ground_truth_mask = (
            ground_truth_mask.clone().detach().squeeze(0)
        )
        ground_truth_mask = F.convert_image_dtype(
            ground_truth_mask, dtype=torch.float
        )

        if self.prompt is not None:
            file_name = os.path.basename(img_filename)

            prompt = next(
                (d[file_name] for d in self.prompt if file_name in d),
                None,
            )
        # print('the prompt is ', prompt)

        else:
            prompt = get_bboxes(ground_truth_mask)

        inputs = self.processors(
            image,
            input_boxes=[[prompt]],
            return_tensors="pt",
            do_rescale=False,
        )

        inputs = {k: v.squeeze(0) for k, v in inputs.items()}
        inputs["ground_truth_mask"] = (ground_truth_mask > 0).float()

        return inputs


class CustomDataLoader:
    def __init__(
        self,
        dataset,
        batch_size=1,
        shuffle=False,
        num_workers=0,
        collate_fn=None,
    ):
        self.dataset = dataset
        self.batch_size = batch_size
        self.shuffle = shuffle
        self.num_workers = num_workers
        self.collate_fn = collate_fn

    def __iter__(self):
        if self.collate_fn is not None:
            return iter(
                self.collate_fn(
                    self.dataset, self.batch_size, self.shuffle
                )
            )
        else:
            return iter(
                self._default_collate(
                    self.dataset, self.batch_size, self.shuffle
                )
            )

    def _default_collate(self, dataset, batch_size, shuffle):
        indices = list(range(len(dataset)))
        if shuffle:
            random.shuffle(indices)
        for batch_start in range(0, len(dataset), batch_size):
            batch_indices = indices[
                batch_start : batch_start + batch_size
            ]
            batch = [dataset[i] for i in batch_indices]
            yield batch

    def __len__(self):
        return len(self.dataset) // self.batch_size + (
            1 if len(self.dataset) % self.batch_size != 0 else 0
        )


def collate_fn(dataset, batch_size, shuffle):
    """
    Custom collate function to handle variable-sized bounding boxes from ground truth masks.

    Args:
        dataset: The dataset containing preprocessed data (image, mask, prompt).
        batch_size: Size of the batch.
        shuffle: Whether to shuffle the data.

    Returns:
        A generator yielding batches, each containing a dictionary with preprocessed data.
    """
    indices = list(range(len(dataset)))
    if shuffle:
        random.shuffle(indices)
    for batch_start in range(0, len(dataset), batch_size):
        batch_indices = indices[
            batch_start : batch_start + batch_size
        ]
        batch = [dataset[i] for i in batch_indices]

        pixel_values = []
        ground_truth_masks = []
        input_boxes = (
            []
        )  # Optional, include if you need bounding boxes explicitly
        original_sizes = (
            []
        )  # Optional, include if you need original sizes
        reshaped_input_sizes = (
            []
        )  # Optional, include if you need reshaped input sizes

        for item in batch:
            pixel_values.append(item["pixel_values"])
            ground_truth_masks.append(item["ground_truth_mask"])
            # Extract bounding boxes from masks if needed (adjust logic based on your mask format)
            input_boxes.append(item["input_boxes"])
            original_sizes.append(
                (item["original_sizes"])
            )  # Convert to tensor
            reshaped_input_sizes.append(
                (item["reshaped_input_sizes"])
            )  # Convert to tensor

        yield {
            "pixel_values": torch.stack(pixel_values),
            "ground_truth_mask": torch.stack(ground_truth_masks),
            "input_boxes": input_boxes,  # Include if you need bounding boxes explicitly
            "original_sizes": torch.stack(original_sizes),
            "reshaped_input_sizes": torch.stack(reshaped_input_sizes),
        }


import torch


class DiceLoss(torch.nn.Module):
    def __init__(self):
        super(DiceLoss, self).__init__()

    def forward(self, input, target):
        smooth = 1e-5
        input_flat = input.view(-1)
        target_flat = target.view(-1)
        intersection = (input_flat * target_flat).sum()
        return 1 - (
            (2.0 * intersection + smooth)
            / (input_flat.sum() + target_flat.sum() + smooth)
        )


def validate_and_modify_path(path, base_path):
    """
    Validate and modify a file path, ensuring it exists

    Args:
        path (str): The input file path to validate.
        base_path (str): The base path to join with 'path' if it
        doesn't exist as-is.

    Returns:
        str: The validated and possibly modified file path.
    """
    if not os.path.exists(path):
        path = os.path.join(base_path, path)
        if not os.path.exists(path):
            raise ValueError(
                f"The path {path} does not exist."
                "Please provide a valid path."
            )
    return path


def add_arguments_from_schema(schema, parser):
    """
    Iterates through the fields defined in a schema and adds
    corresponding commandline arguments to the provided
    ArgumentParser object.

    Args:
        schema (marshmallow.Schema): The schema object containing field
        definitions.
        parser (argparse.ArgumentParser): The ArgumentParser object
        to which arguments will be added.

    Returns:
        None
    """
    for field_name, field_obj in schema.fields.items():
        arg_name = f"--{field_name}"

        arg_kwargs = {
            "help": field_name,
        }

        if isinstance(field_obj, fields.Int):
            arg_kwargs["type"] = int
        elif isinstance(field_obj, fields.Bool):
            arg_kwargs["action"] = (
                "store_false"
                if field_obj.load_default
                else "store_true"
            )

        elif isinstance(field_obj, fields.Float):
            arg_kwargs["type"] = float
        else:
            arg_kwargs["type"] = str

        if field_obj.required:
            arg_kwargs["required"] = True

        if field_obj.load_default and not isinstance(
            field_obj, fields.Bool
        ):
            arg_kwargs["default"] = field_obj.load_default

        if field_obj.metadata.get("description"):
            arg_kwargs["help"] = field_obj.metadata["description"]
        # Debug print statements
        parser.add_argument(arg_name, **arg_kwargs)


def show_mask(mask, ax, random_color=True):
    if random_color:
        color = np.concatenate(
            [np.random.random(3), np.array([0.6])], axis=0
        )
    else:
        color = np.array([30 / 255, 144 / 255, 255 / 255, 0.6])
    h, w = mask.shape[-2:]

    mask_image = mask.reshape(h, w, 1) * color.reshape(1, 1, -1)
    ax.imshow(mask_image)


def show_box(box, ax):
    x0, y0 = box[0], box[1]
    w, h = box[2] - box[0], box[3] - box[1]
    ax.add_patch(
        plt.Rectangle(
            (x0, y0),
            w,
            h,
            edgecolor="green",
            facecolor=(0, 0, 0, 0),
            lw=2,
        )
    )


def show_boxes_on_image(raw_image, boxes):
    plt.figure(figsize=(10, 10))
    plt.imshow(raw_image)
    for box in boxes:
        show_box(box, plt.gca())
    # plt.axis("on")


# plt.show()


def show_points_on_image(raw_image, input_points, input_labels=None):
    plt.figure(figsize=(10, 10))
    plt.imshow(raw_image)
    input_points = np.array(input_points)
    if input_labels is None:
        labels = np.ones_like(input_points[:, 0])
    else:
        labels = np.array(input_labels)
    show_points(input_points, labels, plt.gca())
    plt.axis("on")
    plt.show()


def show_points_and_boxes_on_image(
    raw_image, boxes, input_points, input_labels=None
):
    plt.figure(figsize=(10, 10))
    plt.imshow(raw_image)
    input_points = np.array(input_points)
    if input_labels is None:
        labels = np.ones_like(input_points[:, 0])
    else:
        labels = np.array(input_labels)
    show_points(input_points, labels, plt.gca())
    for box in boxes:
        show_box(box, plt.gca())
    plt.axis("on")
    plt.show()


def show_points_and_boxes_on_image(
    raw_image, boxes, input_points, input_labels=None
):
    plt.figure(figsize=(10, 10))
    plt.imshow(raw_image)
    input_points = np.array(input_points)
    if input_labels is None:
        labels = np.ones_like(input_points[:, 0])
    else:
        labels = np.array(input_labels)
    show_points(input_points, labels, plt.gca())
    for box in boxes:
        show_box(box, plt.gca())
    plt.axis("on")
    plt.show()


def show_points(coords, labels, ax, marker_size=375):
    pos_points = coords[labels == 1]
    neg_points = coords[labels == 0]
    ax.scatter(
        pos_points[:, 0],
        pos_points[:, 1],
        color="green",
        marker="*",
        s=marker_size,
        edgecolor="white",
        linewidth=1.25,
    )
    ax.scatter(
        neg_points[:, 0],
        neg_points[:, 1],
        color="red",
        marker="*",
        s=marker_size,
        edgecolor="white",
        linewidth=1.25,
    )


def show_masks_on_image(
    raw_image, masks, scores, boxes, show_boxs=False
):
    """Displays masks on an image with optional bounding boxes.

    Args:
        raw_image: The original image as a NumPy array.
        masks: A tensor of masks with shape (N, H, W) or (N, 1, H, W).
        scores: A tensor of confidence scores with shape (N,) or (1, N).
        boxes: A tensor of bounding boxes with shape (N, 4).
        show_boxs: Whether to display bounding boxes. Defaults to False.

    Returns:
        A BytesIO buffer containing the image with masks and boxes.
    """

    _, nub_prediction, nb_masks = scores.shape
    print(f"Number of PREDICTION: {nub_prediction}")
    print(f"Number of masks: {masks.shape}")

    if len(masks.shape) == 4:
        masks = masks.squeeze()
    if scores.shape[0] == 1:
        scores = scores.squeeze()

    fig, axes = plt.subplots(
        nb_masks, nub_prediction, figsize=(15, 15)
    )

    buffer = io.BytesIO()  # Create an in-memory buffer

    for j in range(nb_masks):
        for i in range(nub_prediction):
            if nb_masks == 1 and nub_prediction == 1:
                ax = axes
                mask = masks.cpu().detach()
                score = scores
            elif nb_masks > 1:
                ax = axes[j, i] if nub_prediction > 1 else axes[j]
                mask = (
                    masks[i, j].cpu().detach()
                    if nub_prediction > 1
                    else masks[j].cpu().detach()
                )
                score = (
                    scores[i, j].item()
                    if nub_prediction > 1
                    else sscores[j]
                )

            else:
                ax = axes[i]
                mask = masks[i].cpu().detach()
                score = scores[i].item()

            ax.imshow(np.array(raw_image))
            show_mask(mask, ax)
            ax.title.set_text(
                f"Prediction {i+1}, Mask {j+1}, Score: {score:.3f}"
            )
            ax.axis("off")
            if show_boxs:
                show_box(boxes[i], ax)

    fig.savefig(buffer, format="png")
    buffer.seek(0)
    plt.close(fig)
    return buffer


if __name__ == "__main__":
    path = (
        "/srv/Fish_Dataset_Instance_segmentation-1/train/prompt.json"
    )
    img_path = "Fish_Dataset_Instance_segmentation-1/train/masks"
    file_list = sorted(
        glob.glob(img_path + "/*.png")
        + glob.glob(img_path + "/*.jpg")
    )
    prompt_path = os.path.join(
        "Fish_Dataset_Instance_segmentation-1/train/",
        "prompt",
        "prompt.json",
    )
    if os.path.exists(prompt_path):
        with open(prompt_path, "r") as file:

            prompt = json.load(file)
    for index, img in enumerate(file_list):

        print(img)
        file_name = os.path.basename(img)
        box = next(
            (d[file_name] for d in prompt if file_name in d), None
        )
        print(box)
    print(len(file_list))
