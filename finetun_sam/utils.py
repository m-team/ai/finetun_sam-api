"""Package to create datasets, pipelines and other utilities.

You can use this module for example, to write all the functions needed to
operate the methods defined at `__init__.py` or in your scripts.

All functions here are optional and you can add or remove them as you need.
"""

import glob
import logging


# from finetun_sam import config
from torch.utils.data import Dataset
import numpy as np

import os
import numpy as np
import torch
import matplotlib.pyplot as plt
import torchvision.transforms.functional as F
from torchvision.ops import masks_to_boxes
from torchvision.io import read_image, ImageReadMode
import torchvision
from marshmallow import fields
import albumentations as A
import matplotlib.pyplot as plt
import numpy as np
import torch

import io
import os
import json
import numpy as np
from pycocotools import mask as mask_utils
from PIL import Image

logger = logging.getLogger(__name__)

from PIL import Image, ImageDraw
import numpy as np
import json
import os


def coco_to_masks(json_file, save_dir, original_image_dir):
    """
    Processes a COCO format JSON file to generate binary mask images and corresponding bounding box prompts.

    Args:
        json_file (str): Path to the COCO JSON file containing image annotations.
        save_dir (str): Directory where the generated mask images and the bounding box prompts JSON file will be saved.
        original_image_dir (str): Directory containing the original images referenced in the COCO JSON file.
    """
    with open(json_file, "r") as f:
        coco_data = json.load(f)

    # Create save directory if it doesn't exist

    mask_path = os.path.join(save_dir, "masks")
    os.makedirs(mask_path, exist_ok=True)
    images = coco_data["images"]
    annotations = coco_data["annotations"]
    prompt = []
    for image in images:
        width, height = image["width"], image["height"]
        mask_image = Image.new("L", (width, height), 0)
        object_number = 1
        bboxs = []
        for ann in annotations:

            if ann["image_id"] == image["id"]:
                bbox = [
                    ann["bbox"][0],
                    ann["bbox"][1],
                    ann["bbox"][2] + ann["bbox"][0],
                    ann["bbox"][3] + ann["bbox"][1],
                ]
                bboxs.append(bbox)

                for seg in ann["segmentation"]:
                    # Convert polygons to a binary mask and add it to the main mask
                    poly = (
                        np.array(seg).reshape((-1, 2)).tolist()
                    )  # Ensure correct shape
                    poly = [tuple(p) for p in poly]
                    ImageDraw.Draw(mask_image).polygon(
                        poly, outline=0, fill=1
                    )

                object_number += 1  # Increment object number

        prompt.append({image["file_name"]: bboxs})
        filename = image["file_name"]
        print(f"the bbox for file {filename} is {bboxs}")
        # Save mask image
        original_image_path = os.path.join(
            original_image_dir, image["file_name"]
        )

        new_image_path = os.path.join(
            save_dir, "masks", os.path.basename(original_image_path)
        )

        mask_image.save((new_image_path))

    label_path = os.path.join(save_dir, "prompt")

    os.makedirs(label_path, exist_ok=True)
    with open(os.path.join(label_path, "prompt.json"), "w") as file:
        json.dump(prompt, file)


def show_mask(mask, ax, random_color=True):
    if random_color:
        color = np.concatenate(
            [np.random.random(3), np.array([0.6])], axis=0
        )
    else:
        color = np.array([30 / 255, 144 / 255, 255 / 255, 0.6])
    h, w = mask.shape[-2:]

    mask_image = mask.reshape(h, w, 1) * color.reshape(1, 1, -1)
    ax.imshow(mask_image)


def show_masks_on_image(
    raw_image, masks, scores, boxes, show_boxs=False
):
    """Displays masks on an image with optional bounding boxes.

    Args:
        raw_image: The original image as a NumPy array.
        masks: A tensor of masks with shape (N, H, W) or (N, 1, H, W).
        scores: A tensor of confidence scores with shape (N,) or (1, N).
        boxes: A tensor of bounding boxes with shape (N, 4).
        show_boxs: Whether to display bounding boxes. Defaults to False.

    Returns:
        A BytesIO buffer containing the image with masks and boxes.
    """

    _, nub_prediction, nb_masks = scores.shape
    print(f"Number of PREDICTION: {nub_prediction}")
    print(f"Number of masks: {masks.shape}")

    if len(masks.shape) == 4:
        masks = masks.squeeze()
    if scores.shape[0] == 1:
        scores = scores.squeeze()

    fig, axes = plt.subplots(
        nb_masks, nub_prediction, figsize=(15, 15)
    )

    buffer = io.BytesIO()  # Create an in-memory buffer

    for j in range(nb_masks):
        for i in range(nub_prediction):
            if nb_masks == 1 and nub_prediction == 1:
                ax = axes
                mask = masks.cpu().detach()
                score = scores
            elif nb_masks > 1:
                ax = axes[j, i] if nub_prediction > 1 else axes[j]
                mask = (
                    masks[i, j].cpu().detach()
                    if nub_prediction > 1
                    else masks[j].cpu().detach()
                )
                score = (
                    scores[i, j].item()
                    if nub_prediction > 1
                    else sscores[j]
                )

            else:
                ax = axes[i]
                mask = masks[i].cpu().detach()
                score = scores[i].item()

            ax.imshow(np.array(raw_image))
            show_mask(mask, ax)
            ax.title.set_text(
                f"Prediction {i+1}, Mask {j+1}, Score: {score:.3f}"
            )
            ax.axis("off")
            if show_boxs:
                show_box(boxes[i], ax)
    plt.show()
    fig.savefig(buffer, format="png")
    buffer.seek(0)
    plt.close(fig)
    return buffer


if __name__ == "__main__":

    json_file = "Fish_Dataset_Instance_segmentation-1/train/_annotations.coco.json"
    save_dir = "Fish_Dataset_Instance_segmentation-1/train/"

    coco_to_masks(
        json_file,
        save_dir,
        "Fish_Dataset_Instance_segmentation-1/train/",
    )

    # image=Image.open('/srv/Fish_Dataset_Instance_segmentation-1/test/00901_png.rf.4cae1e392d8ae314e6b8ca112b433a1d.jpg')
    # show_masks_on_image(image, masks, scores=np.array([[[100]]]),boxes=None)
